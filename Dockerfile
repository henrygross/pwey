FROM node:6.10.3-alpine
COPY src /
WORKDIR /
EXPOSE 80
RUN ["apk", "update"]
RUN ["apk", "add", "git", "tar", "bzip2", "wget", "ca-certificates"]
RUN src/checkout 1501876642
CMD ["node", "app.js"]
